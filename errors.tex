%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}
\makeatletter
\def\verbatim@font{\fontfamily{zi4}\selectfont\color{midnightblue}}
\makeatother




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \Fontskrivan\huge\bfseries\color{red} Error Analysis  }\\[2mm]
\end{center}
\hrule
\vskip 0.2mm
\hrule


\vskip 1.0cm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%            E R R O R S   F O R   C A L C U L A T E D   V A L U E S           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Errors for calculated values}}

	Uncertainties are normally denoted by symbol \cc{ \Delta } in experimental physics ---
	for instance, \cc{ \Delta m }, \cc{ \Delta l }, \cc{ \Delta t }.
	This is an overloaded use of the symbol, since you may be used to the fact
	that \cc{ \Delta x } usually means a \emph{change} in \cc{ x }.
	Let us hope that such an amiguity does not arise, and that at any given moment
	you are going to know whether you are dealing with an uncertainty or a change of a quantity.
	In this discussion symbol \cc{ \Delta } is entirely dedicated to be used as an uncertainty
	of a number
	
\bigskip\noindent
	Assume you have measured quantities \cc x and \cc y, for which you have
	estimates of their errors --- \cc{ \Delta x } and \cc{ \Delta y }, correspondingly.
	They can include a length, time, mass, force, temperature, pressure and what not.
	Based on these two quantities you have calculated your experimental result ---
	we will call it \cc{ f }.
	That is to say,
\ccc
\beq
	f ~~=~~ f(x,\, y)\ccb\,.
\eeq
\ccb
	If you have more than two variables, the analysis is completely the same, so
	we will just keep two for simplicity.
	The error for quantity \cc{ f } is obtained by calculating the \emph{partial derivatives}
	\cc{ \partial f / \partial x } and \cc{ \partial f / \partial y },
	via the following kind of ``pythagorean'' formula,
\ccc
\beq
	(\Delta f)^2	~~=~~	\Big (\, \frac{\partial f}{\partial x}\, \Delta x \,\Big)^2
				~+~
				\Big (\, \frac{\partial f}{\partial y}\, \Delta y \,\Big)^2
	\ccb\,.
\eeq
\ccb
	This gives you a rule how to calculate an error for a quantity which is just \emph{a sum}
	of two variables,
\ccc
\beq
\label{sumrule}
	f(x)	~~=~~	x  ~+~  y
	\qquad\qquad	\Longrightarrow	\qquad\qquad
	(\Delta f)^2	~~=~~	(\Delta x)^2  ~+~  (\Delta y)^2
	\ccb\,.
\eeq
\ccb
	This result is trivially augmented if \cc x or \cc y are multiplied by exact constants,
\ccc
\beq
	f(x)	~~=~~	3\,x  ~+~  5\,y
	\qquad\qquad	\Longrightarrow	\qquad\qquad
	(\Delta f)^2	~~=~~	(3 \Delta x)^2  ~+~  (5 \Delta y)^2
	\ccb\,,
\eeq
\ccb
	and similarly for any other exact constants.
	Here are a couple of other examples and their corresponding uncertainties:
\ccc
\begin{align*}
%
	&
	f(x)	~~=~~	e^x
	&&
	\Longrightarrow
	&&
	\Delta f	~~=~~	e^x\, |\Delta x|
	\ccb\,,
	\\[4mm]
%
	&
	f(x)	~~=~~	\ln \frac x b
	&&
	\Longrightarrow
	&&
	\Delta f	~~=~~	\Big| \frac {\Delta x} x \Big|
	\ccb\,.
\end{align*}
\ccb

\bigskip\noindent
	If a quantity \cc f happens to be a \emph{product} or a \emph{ratio} of its variables,
	then the ``pythagorean'' formula works for the \emph{relative} errors \cc{ \Delta x / x },
	\cc{ \Delta y / y } rather than for the absolute errors,
\ccc
\beq
\label{productrule}
	f	~~=~~	a\,x\,y
	\ccb\quad\text{or}\quad\ccc
	a\,x\,/\,y
	\qquad\qquad\Longrightarrow\qquad\qquad
	\Big(\, \frac{\Delta f} f \,\Big)^2
		~~=~~
	\Big(\, \frac{\Delta x} x \,\Big)^2
		~+~
	\Big(\, \frac{\Delta y} y \,\Big)^2
	\ccb\,.
\eeq
\ccb

\bigskip\noindent
	In particular, if a quantity includes a square or any other power of variable \cc x, then
	that power effectively multiplies the corresponding uncertainty term,
\ccc
\beq
\label{power}
	f	~~=~~	x^3\,y
	\qquad\qquad\Longrightarrow\qquad\qquad
	\Big(\, \frac{\Delta f} f \,\Big)^2
		~~=~~
	3\,\Big(\, \frac{\Delta x} x \,\Big)^2
		~+~
	\Big(\, \frac{\Delta y} y \,\Big)^2
	\ccb\,.
\eeq
\ccb
	This is because we can think of \cc{ x^3 } as of a product of three ``independent''
	variables \cc{ x \cdot x \cdot x }, which all happen to have the same name.
	For each of them we have to include a relative error (\cc{ \Delta x / x}),
	and thus we have a factor of three.
	This works for non--integer powers either --- say, for a square root

\bigskip\noindent
	If the resulting quantity happens to be a \emph{combination} of different operations, say
\ccc
\beq
	f	~~=~~	x\,y  ~+~  y \,/\, z
	\ccb\,,
\eeq
\ccb
	then the uncertainties are calculated separately for each step.
	In other words, you could introduce temporary quantities \cc P and \cc Q,
\ccc
\beq
	f	~~=~~	P  ~+~  Q
	\ccb\,,
	\qquad\text{where}\qquad
	\ccc
	P	~~=~~	x\,y
	\ccb\qquad\text{and}\qquad\ccc
	Q	~~=~~	y \,/\, z
	\ccb\,,
\eeq
\ccb
	and first calculate \cc{ \Delta P } and \cc{ \Delta Q }
	from the product rule \eqref{productrule},
	and then use \cc P and \cc Q as variables to calculate
	\cc{ \Delta f } from the sum rule \eqref{sumrule},
\ccc
\beq
	(\, \Delta f \,)^2	~~=~~	(\, \Delta P \,)^2  ~+~  (\, \Delta Q \,)^2
	\ccb\,.
\eeq
\ccb


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                     S I M P L I F I E D   F O R M U L A S                    %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Simplified formulas}}

	In introductory--level laboratories, the ``pythagorean'' formulas \eqref{sumrule}, \eqref{productrule}
	are often traded for their somewhat simplified analogues.
	That is, instead of squaring uncertainties, adding them up, and then taking the square root,
	one uses
\ccc
\beq
\label{sumrule}
	f(x)	~~=~~	x  ~+~  y
	\qquad\qquad	\Longrightarrow	\qquad\qquad
	\Delta f	~~=~~	\Delta x  ~+~  \Delta y
	\ccb\,,
\eeq
\ccb
	for the sum rule, and
\ccc
\beq
\label{productrule}
	f	~~=~~	a\,x\,y
	\ccb\quad\text{or}\quad\ccc
	a\,x\,/\,y
	\qquad\qquad\Longrightarrow\qquad\qquad
	\frac{\Delta f} f
		~~=~~
	\frac{\Delta x} x
		~+~
	\frac{\Delta y} y
	\ccb\,,
\eeq
\ccb
	for the product rule.
	Constant \cc{ a }, again, is assumed to be an exact quantity here (\emph{i.e.} having zero uncertainty).
	Analogously to \eqref{power}, if a quantity enters as a power, then
	its uncertainty receives a corresponding factor, \emph{e.g.}
\ccc
\beq
	f	~~=~~	\sqrt x\,y
	\qquad\qquad\Longrightarrow\qquad\qquad
	\frac{\Delta f} f
		~~=~~
	\frac 1 2\, \frac{\Delta x} x
		~+~
	\frac{\Delta y} y
	\ccb\,.
\eeq
\ccb
	This way, the uncertainties are easier and faster to calculate,
	but the results for \cc{ \Delta f } are somewhat overestimated.
	This is acceptable, however, for introductory level laboratories


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                          Q U O T I N G   E R R O R S                         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Quoting errors}}

	Quantities that are derived from experiment, are usually found
	using a calculator or other gadgets.
	They typically give you multiple significant figures,
	say \cc{ f ~=~ \SI{39.542781}{\metre\per\second} }.
	It is highly unlikely, however, that your experiment has been conducted
	with an accuracy to the sixth or seventh figure.
	In that case, one \emph{must} round the result down to the number
	that is actually \emph{affected} by the error,
\ccc
\beq
	f	~~=~~	39.542781 ~\pm~ \SI{2.43972318}{\metre\per\second}
	\qquad\quad\ccb\Longrightarrow\ccc\qquad\quad
	39.5 ~\pm~ 2.5	\qquad\ccb\text{or better}\ccc\qquad
	40 ~\pm~ \SI{2.5}{\metre\per\second}
	\ccb\,.
\eeq
\ccb
	Formal rules suggest to keep one figure after the one affected by the error,
	but in practice that is unnecessary (so we rounded \cc{ 39.5 } to \cc{ 40 }).
	Notice how we also rounded the error to \cc{ 2.5 } and not \cc{ 2.4 }.
	Error of \cc{ 2.5 } has a better visual readability than \cc{ 2.4 },
	while rounding it up to \cc{ 3.0 } would actually be a bit of a stretch

\bigskip\noindent
	Another example,
\ccc
\beq
	f	~~=~~	132 ~\pm~ \SI{53}{\centi\metre}
	\qquad\qquad\ccb\Longrightarrow\ccc\qquad\qquad
	130 ~\pm~ \SI{50}\metre
	\ccb\,.
\eeq
\ccb
	The error also rounds itself, especially if it is large compared to the actual quantity.
	The \emph{main message} is, that the result should be \emph{visually easy to comprehend},
	even if this involves rounding things up in a more crude way than one typically does.
	The errors are \emph{approximate quantities} anyway, and it does not hurt much to round them.
	Agree, that \cc{ 130 ~\pm~ \SI{50}{\centi\metre} } visually reads a lot better than 
	\cc{ 132 ~\pm~ \SI{47.3}{\centi\metre} },
	while no essential information is really lost in the process of this rounding


\end{document}
